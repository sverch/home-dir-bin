#!/bin/bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

MIN_ARGS_REQUIRED=1
MAX_ARGS_REQUIRED=1
if [ $# -lt "${MIN_ARGS_REQUIRED}" ] || [ $# -gt "${MAX_ARGS_REQUIRED}" ]; then
	cat <<EOF
Usage: $0 <backup_destination> [<backup_config>]

    Backs up to <backup_destination> based on the configuration in
    \$HOME/.sverch-backup.yaml or in [<backup_config>].

    The format of the config is just a list of directories to back up, and a
    name override (hostname is the default). You can also exclude specific
    directories:

        backup:
          - ~/Photos
          - ~/projects
          - ~/personal
        exclude:
          - ~/personal/private
        name: mybackup

   The format of the backup medium is:

       backups/vaughan/
       ├── data
       │   ├── 2019-08-10
       │   │   └── sverch
       │   └── latest -> 2019-08-10
       ├── logs
       │   └── 2019-08-10.log
       └── README.txt

    The path starts with "backups" and the hostname of the machine being backed
    up, or a name override. Within that directory are "data", the actual backups
    with timestamps, and "logs", the log output of the backup on the
    corresponding date. This script will also make a symlink to the latest
    backup. In this case the entire home directory was backed up, so there's
    just one directory, "sverch".

    To save space on subsequent runs, this script will first look for existing
    backups, and then use the "-l" option to "cp" to create a copy. This will
    "copy" the backup, but only create hard links to the files, rather than
    actually copying the data. Then, when rsync is run, only the delta needs to
    be transferred, and only the delta will take up more space.

EOF
	exit 1
fi

TARGET=$1
DEFAULT_CONFIG=$HOME/.sverch-backup.yaml
CONFIG=${2:-$DEFAULT_CONFIG}

run() {
	echo "+" "$@" 1>&2
	"$@"
}

color() {
	COLOR=$1
	MESSAGE=$2
	case "${COLOR}" in
	red)
		echo -e "\e[31m${MESSAGE}\e[39m"
		;;
	blue)
		echo -e "\e[94m${MESSAGE}\e[39m"
		;;
	green)
		echo -e "\e[32m${MESSAGE}\e[39m"
		;;
	*)
		echo "Unrecognized color: ${COLOR}" 1>&2
		echo -e "${MESSAGE}"
		;;
	esac
}

# https://stackoverflow.com/a/29310477
expandPath() {
	local path
	local -a pathElements resultPathElements
	IFS=':' read -r -a pathElements <<<"$1"
	: "${pathElements[@]}"
	for path in "${pathElements[@]}"; do
		: "$path"
		case $path in
		"~+"/*)
			path=$PWD/${path#"~+/"}
			;;
		"~-"/*)
			path=$OLDPWD/${path#"~-/"}
			;;
		"~"/*)
			path=$HOME/${path#"~/"}
			;;
		"~"*)
			username=${path%%/*}
			username=${username#"~"}
			IFS=: read -r _ _ _ _ _ homedir _ < <(getent passwd "$username")
			if [[ $path = */* ]]; then
				path=${homedir}/${path#*/}
			else
				path=$homedir
			fi
			;;
		esac
		resultPathElements+=("$path")
	done
	local result
	printf -v result '%s:' "${resultPathElements[@]}"
	printf '%s\n' "${result%:}"
}

symlink_latest() {
	local latest_backup_path
	local current_backup_path
	latest_backup_path=$1
	current_backup_path=$2
	run ln -sfn "$(basename "$current_backup_path")" "$latest_backup_path"
}

backup_dirs() {
	yq r "${CONFIG}" 'backup[*]'
}

exclude_dirs() {
	yq r "${CONFIG}" 'exclude[*]'
}

backup_name() {
	yq r "${CONFIG}" --defaultValue "$(hostname)" 'name'
}

# Copy config to the backup (with no dot) so we know what config was used.
copy_config() {
	local current_backup_path
	current_backup_path=$1
	cp "${CONFIG}" "${current_backup_path}/sverch-backup.yaml"
}

# Make the directory structure if this is the first backup
ensure_directory_structure() {
	local base_dir
	base_dir=$1

	run mkdir -p "$base_dir"

	if [ ! -e "$base_dir/README.txt" ]; then
		run cat <<EOF >"$base_dir/README.txt"
This directory was originally generated with $0.  Do not make manual modifications to anything in
this directory.
EOF
	fi

	if [ ! -e "$base_dir/logs" ]; then
		run mkdir "$base_dir/logs"
	fi

	if [ ! -e "$base_dir/data" ]; then
		run mkdir "$base_dir/data"
	fi
}

# Make sure we don't already have data in the place we're backing up to
check_noclobber() {
	local current_backup_path
	current_backup_path=$1
	if [ -e "$current_backup_path" ]; then
		color red "Failed to backup, destination directory [$current_backup_path] already exists!"
		exit 1
	fi
}

# Copy the last backup if it exists, so we only have to fetch the delta.  Note the trailing
# slashes are important to avoid just copying the symlink.
# The -l hard links files to prevent unnecessary disk usage.
copy_latest_if_exists() {
	local latest_backup_path
	local current_backup_path
	latest_backup_path=$1
	current_backup_path=$2
	if [ -e "$latest_backup_path" ]; then
		color green "Found existing backup at $current_backup_path."
		color green "Copying to new location at $current_backup_path so that we only need to transfer the delta."
		run cp -al "$latest_backup_path/" "$current_backup_path/"
	else
		color green "No existing backup found.  Creating backup directory at $current_backup_path."
		run mkdir "$current_backup_path"
	fi
}

main() {
	BASE_DIR=$TARGET/backups/$(backup_name)
	BACKUP_TIMESTAMP=$(date -Iseconds)
	LATEST_BACKUP=$BASE_DIR/data/latest
	CURRENT_BACKUP=$BASE_DIR/data/$BACKUP_TIMESTAMP
	CURRENT_LOGFILE=$BASE_DIR/logs/$BACKUP_TIMESTAMP.log

	ensure_directory_structure "${BASE_DIR}"
	check_noclobber "${CURRENT_BACKUP}"
	copy_latest_if_exists "${LATEST_BACKUP}" "${CURRENT_BACKUP}"

	EXCLUDE_OPTS=()
	for dir in $(exclude_dirs); do
		EXCLUDE_OPTS=("${EXCLUDE_OPTS[@]}" --exclude "$(expandPath "${dir}")")
	done

	BACKUP_DIRS=()
	for dir in $(backup_dirs); do
		BACKUP_DIRS=("${BACKUP_DIRS[@]}" "$(expandPath "${dir}")")
	done

	# -a: archive mode - makes rsync recursive and preserves things such as group, owner, time, and
	# whether something is a symlink
	# -v: verbose
	# -h: output numbers in human readable format
	# -x: do not cross filesystem boundaries
	# --delete: delete files that no longer exist on remote
	# -e ssh: use ssh for copy
	# -z compress: compression while copying
	color green "Backing up $(backup_dirs) to $CURRENT_BACKUP...."
	color green "Logfile at $CURRENT_LOGFILE"
	run rsync --delete "${EXCLUDE_OPTS[@]}" -zxavhe ssh "${BACKUP_DIRS[@]}" "$CURRENT_BACKUP" >>"$CURRENT_LOGFILE"

	copy_config "${CURRENT_BACKUP}"
	symlink_latest "${LATEST_BACKUP}" "${CURRENT_BACKUP}"
}
main
