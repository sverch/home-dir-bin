#!/bin/bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

if [ $# -ne 2 ]; then
    echo "Usage: $0 [source] [destination]"
    exit 1;
fi

run () {
    echo "+ " "$@" 1>&2
    "$@"
}

SOURCE=$1
DESTINATION=$2
LOGFILE="/tmp/rsync-$(date -Iseconds).log"

# -a: archive mode - makes rsync recursive and preserves things such as group, owner, time, and
# whether something is a symlink
# -v: verbose
# -h: output numbers in human readable format
# -x: do not cross filesystem boundaries
# --delete: delete files that no longer exist on remote
# -e ssh: use ssh for copy
# -z compress: compression while copying
echo "Making $SOURCE and $DESTINATION the union of the files in both...."
echo "Logfile at $LOGFILE"
run rsync -zxavhe ssh "$SOURCE/" "$DESTINATION" | tee -a "$LOGFILE"
run rsync -zxavhe ssh "$DESTINATION/" "$SOURCE" | tee -a "$LOGFILE"
echo "Logfile at $LOGFILE"
