#!/bin/bash -e

# Script to backup my home directory to an external hard drive.

# The destination must be on a different filesystem, otherwise this is bad.  rsync is using -x which
# tells it to not cross filesystem boundaries.
"$(cd "$(dirname "$0")" && pwd)"/rsync_backup.sh ~ ~/toshiba_1tb/backups/"$(hostname)"
