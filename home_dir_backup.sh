#!/bin/bash -e

# TODO: Read this: http://www.mikerubel.org/computers/rsync_snapshots/
# Backup [source] to [destination] in a "snapshot like" method.  Will timestamp the data directory
# based on today's date, copy the last snapshot and use rsync to only copy a delta into the new data
# (deleting things that were deleted on the remote).

# TODO: Make this more generic.  Right now it only supports local->local or remote->local.  Or maybe
# just make a "rotate backup" script that does most of this logic.

if [ $# -ne 2 ]; then
    echo "Usage: $0 [source] [destination]"
    exit 1;
fi

run () {
    echo "+ " "$@" 1>&2
    "$@"
}

SOURCE=$1
DESTINATION=$2

BASE_DIR=$DESTINATION

# TODO: Change this if timestamping based on day becomes a problem
TODAY=$(date -I)
NEXT_BACKUP=$BASE_DIR/data/$TODAY
LATEST_BACKUP=$BASE_DIR/data/latest
CURRENT_LOGFILE=$BASE_DIR/logs/${TODAY}.log
EXCLUDE_OPTS=(--exclude ".tox" \
    --exclude "/*/.vagrant.d/" \
    --exclude "/*/.snapshots/" \
    --exclude "/*/.minikube/" \
    --exclude "/*/.docker/" \
    --exclude "/*/.tfenv/" \
    --exclude "/*/.bosh/" \
    --exclude "/*/go" \
    --exclude "/*/ps" \
    --exclude "/*/Downloads" \
    --exclude "/*/.adobe" \
    --exclude "/*/.ansible" \
    --exclude "/*/.ansible_galaxy" \
    --exclude "/*/.aws" \
    --exclude "/*/backgrounds" \
    --exclude "/*/.bundle" \
    --exclude "/*/.cache" \
    --exclude "/*/.cargo" \
    --exclude "/*/.endgame" \
    --exclude "/*/.esd_auth" \
    --exclude "/*/.esmtp_queue" \
    --exclude "/*/.fehbg" \
    --exclude "/*/.gce" \
    --exclude "/*/.gem" \
    --exclude "/*/.gimp-2.8" \
    --exclude "/*/.gnome" \
    --exclude "/*/.gnome2" \
    --exclude "/*/.gnupg" \
    --exclude "/*/.ICEauthority" \
    --exclude "/*/.ipython" \
    --exclude "/*/.keras" \
    --exclude "/*/.lesshst" \
    --exclude "/*/.local" \
    --exclude "/*/.macromedia" \
    --exclude "/*/.mozilla" \
    --exclude "/*/.mplayer" \
    --exclude "/*/.npm" \
    --exclude "/*/.nvidia-settings-rc" \
    --exclude "/*/.odbc.ini" \
    --exclude "/*/.packer.d" \
    --exclude "/*/.pki" \
    --exclude "/*/.pylint.d" \
    --exclude "/*/.rnd" \
    --exclude "/*/.rubies" \
    --exclude "/*/.snapshots" \
    --exclude "/*/.ssh" \
    --exclude "/*/.sslmate" \
    --exclude "/*/.terraform.d" \
    --exclude "/*/.texlive2017" \
    --exclude "/*/.thumbnails" \
    --exclude "/*/.tox" \
    --exclude "/*/.vagrant.d" \
    --exclude "/*/VirtualBox VMs" \
    --exclude "/*/.wget-hsts" \
    --exclude "/*/.wine")

# Make the directory structure if this is the first backup
run mkdir -p "$BASE_DIR"

if [ ! -e "$BASE_DIR/README.txt" ]; then
    run cat << EOF > "$BASE_DIR/README.txt"
This directory was originally generated with $0.  Do not make manual modifications to anything in
this directory.
EOF
fi

if [ ! -e "$BASE_DIR/logs" ]; then
    run mkdir "$BASE_DIR/logs"
fi

if [ ! -e "$BASE_DIR/data" ]; then
    run mkdir "$BASE_DIR/data"
fi

# Make sure we don't already have data there
if [ -e "$NEXT_BACKUP" ]; then
    echo "Failed to backup, destination directory [$NEXT_BACKUP] already exists!"
    exit 1
fi

if [ -e "$LATEST_BACKUP" ]; then
    # Copy the last backup if it exists, so we only have to fetch the delta.  Note the trailing
    # slashes are important to avoid just copying the symlink.
    # The -l hard links files to prevent unnecessary disk usage.
    echo "Found existing backup at $LATEST_BACKUP."
    echo "Copying to new location at $NEXT_BACKUP so that we only need to transfer the delta."
    run cp -al "$LATEST_BACKUP/" "$NEXT_BACKUP/"
else
    echo "No existing backup found.  Creating backup directory at $NEXT_BACKUP."
    run mkdir "$NEXT_BACKUP"
fi

# -a: archive mode - makes rsync recursive and preserves things such as group, owner, time, and
# whether something is a symlink
# -v: verbose
# -h: output numbers in human readable format
# -x: do not cross filesystem boundaries
# --delete: delete files that no longer exist on remote
# -e ssh: use ssh for copy
# -z compress: compression while copying
# TODO: Rename the old logfile or fail if it already exists
# TODO: Exclude obvious things like ".cache".
echo "Backing up $SOURCE to $NEXT_BACKUP...."
echo "Logfile at $CURRENT_LOGFILE"
run rsync --delete "${EXCLUDE_OPTS[@]}" -zxavhe ssh "$SOURCE" "$NEXT_BACKUP" >> "$CURRENT_LOGFILE"

# Make latest point to the backup we just took
# TODO: This only works if the destination is local
run ln -sfn "$(basename "$NEXT_BACKUP")" "$LATEST_BACKUP"
