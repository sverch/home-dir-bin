#!/bin/bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

# https://stackoverflow.com/a/246128
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

run () {
    echo "+" "$@" 1>&2
    "$@"
}

color () {
    COLOR=$1
    MESSAGE=$2
    case "${COLOR}" in
    red)
        echo -e "\e[31m${MESSAGE}\e[39m"
        ;;
    blue)
        echo -e "\e[94m${MESSAGE}\e[39m"
        ;;
    green)
        echo -e "\e[32m${MESSAGE}\e[39m"
        ;;
    *)
        echo "Unrecognized color: ${COLOR}" 1>&2
        echo -e "${MESSAGE}"
        ;;
    esac
}

run mkdir -p "${HOME}/bin"
for bin_src in "${SCRIPT_DIR}"/*; do
    bin=$(basename "${bin_src}")
    bin_dest="${HOME}/bin/${bin}"
    if [[ "${bin}" == "README.md" ]] ||
        [[ "${bin}" == "install.sh" ]]; then
        continue
    fi
    color green "Checking if \"${bin_dest}\" is installed."
    if [[ -e "${bin_dest}" ]]; then
        if [[ -L "${bin_dest}" ]]; then
            if [[ "$(readlink -f "${bin_dest}")" != "${bin_src}" ]]; then
                color green "\"${bin_dest}\" is a symlink to \"$(readlink -f "${bin_dest}")\"."
                color green "Deleting \"${bin_dest}\""
                run rm -f "${bin_dest}"
                color green "Symlinking \"${bin_dest}\" to \"${bin_src}\""
                run ln -s "${bin_src}" "${bin_dest}"
            fi
        else
            color green "\"${bin_dest}\" already exists and is not a symlink!"
            color green "Backing up to \"${bin_dest}.$(date +%s%N)\"..."
            run mv "${bin_dest}" "${bin_dest}.$(date +%s%N)"
        fi
    else
        if [[ -L "${bin_dest}" ]]; then
	    # Broken Symlink
            if [[ "$(readlink -f "${bin_dest}")" != "${bin_src}" ]]; then
                color green "\"${bin_dest}\" is a symlink to \"$(readlink -f "${bin_dest}")\"."
                color green "Deleting \"${bin_dest}\""
                run rm -f "${bin_dest}"
                color green "Symlinking \"${bin_dest}\" to \"${bin_src}\""
                run ln -s "${bin_src}" "${bin_dest}"
            fi
        else
	    color green "Symlinking \"${bin_dest}\" to \"${bin_src}\""
	    run ln -s "${bin_src}" "${bin_dest}"
	fi
    fi
done

color green ""
color green "Home dir bin scripts installed!"
color green ""
