# home-dir-bin

Some utilities that I've found useful in the past and put in "${HOME}/bin".

Run `./install.sh` to install and add `export PATH="${HOME}/bin:${PATH}"` to
your path.  It'll backup anything that is conflicting.
