#!/bin/sh

###########################################################################
#
# Usage: system-low-battery
#
# Checks if the battery level is low. If “low_threshold” is exceeded
# a system notification is displayed, if “critical_threshold” is exceeded
# a popup window is displayed as well. If “OK” is pressed, the system
# shuts down after “timeout” seconds. If “Cancel” is pressed the script
# does nothing.
#
# This script is supposed to be called from a cron job.
#
###########################################################################

# This is required because the script is invoked by cron. Dbus information
# is stored in a file by the following script when a user logs in. Connect
# it to your autostart mechanism of choice.
#
# #!/bin/sh
# touch $HOME/.dbus/Xdbus
# chmod 600 $HOME/.dbus/Xdbus
# env | grep DBUS_SESSION_BUS_ADDRESS > $HOME/.dbus/Xdbus
# echo 'export DBUS_SESSION_BUS_ADDRESS' >> $HOME/.dbus/Xdbus
#
if [ -r ~/.dbus/Xdbus ]; then
  # shellcheck source=/dev/null
  . ~/.dbus/Xdbus
fi

low_threshold=${LOW_BATTERY_THRESHOLD:-15}
critical_threshold=${CRITICAL_BATTERY_THRESHOLD:-7}
timeout=59
shutdown_cmd='/usr/bin/systemctl suspend'

level=$(acpi | cut -f2 -d, | cut -f1 -d%)
state=$(acpi | cut -f1 -d, | cut -f3 -d' ')

if [ x"$state" != x'Discharging' ]; then
  exit 0
fi

do_shutdown() {
  sleep $timeout && kill "$zenity_pid" 2>/dev/null

  if [ x"$state" != x'Discharging' ]; then
    exit 0
  else
    $shutdown_cmd &>> /home/sverch/shutdown_results.txt
  fi
}

if [ "$level" -gt "$critical_threshold" ] && [ "$level" -lt "$low_threshold" ]; then
  DISPLAY=:1 zenity --warning --text "Battery level is low: $level%"
fi

if [ "$level" -le "$critical_threshold" ]; then

  DISPLAY=:1 zenity --warning --text "Battery level is low: $level%. The system is going to shut down in 1 minute."

  DISPLAY=:1 zenity --question --ok-label 'OK' --cancel-label 'Cancel' \
    --text "Battery level is low: $level%.\n\n The system is going to shut down in 1 minute." &
  zenity_pid=$!

  do_shutdown &
  shutdown_pid=$!

  trap 'kill $shutdown_pid' 1

  if ! wait $zenity_pid; then
    kill $shutdown_pid 2>/dev/null
  fi

fi

exit 0
