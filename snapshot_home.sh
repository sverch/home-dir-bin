#!/bin/bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

if [[ $# != 0 ]] ; then
    echo "Usage: $0"
    echo "   Backs up current user's home directory into /home/<user>/.snapshots."
    echo "   Makes hardlinks wherever possible to not use additional disk space."
    exit 1
fi

run() {
    echo >&2 "+ $*"
    "$@"
}

# -a: archive mode - makes rsync recursive and preserves things such as group, owner, time, and
# whether something is a symlink
# -v: verbose
# -h: output numbers in human readable format
# -x: do not cross filesystem boundaries
# --link-dest: Directory that rsync will look at to see if it can find any
# identical files (in this case they all will be identical because they will all
# have the same path) and create hardlinks if it finds that they are.  This is
# what makes this an actual "snapshot" and not a copy.
run rsync -xavh \
    --exclude=/*/.snapshots \
    --exclude=/*/.minikube \
    --exclude=/*/.docker \
    --exclude=/*/.poetry \
    --exclude=/*/.tfenv \
    --exclude=/*/.bosh \
    --exclude=/*/go \
    --exclude=/*/docker-data \
    --link-dest="$(dirname "$HOME")" "$HOME" "$HOME/.snapshots/$(date -Iseconds)" > "$HOME/.snapshots/$(date -Iseconds).log"
