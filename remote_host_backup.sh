#!/bin/bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

NUM_ARGS_REQUIRED=2
if [ $# -ne "${NUM_ARGS_REQUIRED}" ]; then
    cat <<EOF
Usage: $0 <host> <user>

    backup remote <host> locally, log in as <user>

EOF
    exit 1
fi

BACKUP_HOST=$1
BACKUP_USER=$2
BACKUP_DIR=$HOME/backups/${BACKUP_HOST}

"$(cd "$(dirname "$0")" && pwd)/rsync_backup.sh" "$BACKUP_USER@$BACKUP_HOST:~" "$BACKUP_DIR"
