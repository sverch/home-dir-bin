#!/bin/bash -e

set -euo pipefail

run () {
    echo "+" "$@" 1>&2
    "$@"
}

EXPECTED_ARGS=1
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
    echo "Usage: $(basename "$0") <host>"
    echo "  Script to restore the latest backup for <host> from an external"
    echo "  hard drive into ~/backups"
    exit $E_BADARGS
fi

HOST=$1

# The destination must be on a different filesystem, otherwise this is bad.  rsync is using -x which
# tells it to not cross filesystem boundaries.
"$(cd "$(dirname "$0")" && pwd)"/rsync_backup.sh "$HOME/toshiba_1tb/backups/$HOST" "$HOME/backups/$HOST"
